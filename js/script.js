window.onload = function () {
    var linkovi = document.getElementsByClassName("link");
    var linkCount = linkovi.length;
    for(var i = 0; i < linkCount; i++){
        linkovi[i].addEventListener("mouseover", function(){
            this.classList.remove('btnShadow');
        });
        linkovi[i].addEventListener("mouseout", function(){
            this.classList.add('btnShadow');
        });
    }


    fetch("https://api.openweathermap.org/data/2.5/weather?q=sabac&units=metric&lang=sr&appid=1d152e0b40f04dc9e5970f72b264a377")
    .then(response => response.json())
    .then(data => popuniVreme(data));
};

function popuniVreme(data){
    var vremeDiv = document.getElementById("vremeContainer");
    var imgSrc = "https://openweathermap.org/img/wn/" + data.weather[0].icon + "@2x.png";
    vremeDiv.innerHTML = Math.round(data.main.temp) + " <sup>o</sup>C " + data.weather[0].description + '<img src="' + imgSrc + '" />';
}